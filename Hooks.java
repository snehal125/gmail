package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

public static WebDriver driver;

@Before
    public void beforeHooks(Scenario scenario) {

		System.out.println("\n-------------------------------------------------");
		System.out.println("Starting Scenario - " + scenario.getName());
		System.out.println("-------------------------------------------------\n");
		
		System.setProperty("webdriver.chrome.driver","C:/Users/Snehal/Downloads/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.gmail.com");
	
    } 
@After
	public void afterHooks(Scenario scenario) throws InterruptedException{

		System.out.println("\n----------------------------------------");
		System.out.println("Scenario - " + scenario.getStatus());
		System.out.println("----------------------------------------\n");

	}
}
