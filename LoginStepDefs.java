package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import seleniumCode.GmailLogin;

public class LoginStepDefs {
	
	GmailLogin obj1;
	public LoginStepDefs() {
		obj1 = new GmailLogin();
	}

	@Given("^User has opened Gmail homepage$")
	public void user_has_opened_Gmail_homepage() throws Throwable {
		obj1.titlecheck();
	}

	@When("^User enters the \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_the_and(String email, String password) throws Throwable {
		obj1.logincreds(email,password);
	}

	@Then("^User able to Login Successfully$")
	public void user_able_to_Login_Successfully() throws Throwable {
		Thread.sleep(4000);
		obj1.verifylogin();
	}
	

	@Then("^User clicks on compose button$")
	public void user_clicks_on_compose_button() throws Throwable {
	}

	@Then("^User adds a \"([^\"]*)\"$")
	public void user_adds_a(String receiver_mail) throws Throwable {
		obj1.compose(receiver_mail);

	}

	@When("^User enters subject and mailbody$")
	public void user_enters_subject_and_mailbody() throws Throwable {
	}

	@Then("^User clicks on send$")
	public void user_clicks_on_send() throws Throwable {
	}

}
