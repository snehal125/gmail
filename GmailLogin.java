package seleniumCode;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import stepDefinitions.Hooks;

	public class GmailLogin{

	static WebDriver driver = Hooks.driver;
	
	public void titlecheck() {
		String actualTitle = driver.getTitle();
		String expectedTitle = "Gmail";
		Assert.assertEquals("Condition true",actualTitle, expectedTitle);
	}
	
	public void logincreds(String email, String password) throws InterruptedException {
		
		Thread.sleep(5000);
		driver.findElement(By.id("identifierId")).sendKeys(email);
		driver.findElement(By.xpath("//span[.='Next']")).click();
		Thread.sleep(5000);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.xpath("//span[.='Next']")).click();		
	}
	
	public void verifylogin() {
		WebElement searchbox=driver.findElement(By.id("aso_search_form_anchor"));
		boolean bool=searchbox.isDisplayed();
		Assert.assertEquals(true, bool);
	}

	public void compose(String email) throws InterruptedException{
		
		WebDriverWait myWaitVar = new WebDriverWait(driver,20);
		
		myWaitVar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Compose')]")));
		//click on compose
		driver.findElement(By.xpath("//div[contains(text(),'Compose')]")).click();
		//select recipient
		driver.findElement(By.name("to")).sendKeys(email);
	    //type the mail subject
		driver.findElement(By.name("subjectbox")).sendKeys("Test email from selenium");
		Thread.sleep(2000);
	    //type the mail
		driver.findElement(By.cssSelector(".Am.Al.editable.LW-avf")).click();
		driver.findElement(By.cssSelector(".Am.Al.editable.LW-avf")).sendKeys("Hi");
	    //send the mail
		driver.findElement(By.xpath("//div[contains(text(),'Send')]")).click();

	}

	public void close(){
		if(driver!=null){
			driver.close();
		}
	}
}