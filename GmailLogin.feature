@test1
Feature: Gmail Action Test
Description: This feature will test the Gmail functionalities

Scenario Outline: Successful Login with Valid Credentials
Given User has opened Gmail homepage
When User enters the "<email>" and "<password>"
Then User able to Login Successfully
Then User clicks on compose button
And User adds a "<receiver_mail>"
When User enters subject and mailbody
Then User clicks on send
Examples:
|email|password|receiver_mail|
|snehalpandya125@gmail.com|abc|snehal.pandya@tcs.com|