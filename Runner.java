package runner;

	import org.junit.runner.RunWith;
	import cucumber.api.CucumberOptions;
	import cucumber.api.junit.Cucumber;

	@RunWith(Cucumber.class)
	@CucumberOptions(
	features="Feature",
	glue="stepDefinitions",
	tags = {"@test1"},
	plugin={"pretty"}
	)
	public class Runner {

}